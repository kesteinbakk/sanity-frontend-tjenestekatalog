import { firstUpper } from "../utils"

const groups = {

  name: 'languageGroups',
  title: 'Language groups',
  type: 'document',
  fields: [
    {
      name: 'name',
      title: 'Name',
      type: 'string',
      validation: Rule => Rule.required(),
    },
  ],
  preview: {
    select: {
      title: 'name'
    },
    prepare: ({ title }) => ({ title: firstUpper(title) })
  }
}

export default groups