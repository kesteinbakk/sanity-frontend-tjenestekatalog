import { firstUpper } from "../utils"

const where = {

  name: 'usedWhere',
  title: 'Used where',
  type: 'document',
  fields: [
    {
      name: 'name',
      title: 'Name',
      type: 'string',
      validation: Rule => Rule.required(),
    },
    {
      name: 'inFiles',
      title: 'Used in files',
      type: 'array',
      of: [{ type: 'string' }]
    },
    {
      name: 'fromVersion',
      title: 'From version',
      type: 'string',

    },
  ],
  preview: {
    select: {
      title: 'name'
    },
    prepare: ({ title }) => ({ title: firstUpper(title) })
  }
}

export default where