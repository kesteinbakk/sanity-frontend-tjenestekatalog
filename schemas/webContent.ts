import { supportedLanguages } from "../settings";

const webContent = {
  name: 'webContent',
  title: 'Public web content',
  type: 'document',
  fields: [

    {
      name: 'name',
      title: 'Name',
      type: 'slug',
    },

    ...supportedLanguages.map(lang => ({
      name: lang.id + 'BlockContent',
      title: lang.name,
      type: 'blockContent',
    })),

    {
      name: 'mainImage',
      title: 'Main image',
      type: 'image',
      options: {
        hotspot: true,
      },
    },

    {
      name: 'publishedAt',
      title: 'Published at',
      type: 'datetime',
      validation: Rule => Rule.required(),
      initialValue: (new Date()).toISOString()
    },
  ],

  preview: {
    select: {
      title: 'name.current',
      //subtitle: `${defLangId}Content.title`,
      media: 'mainImage',
    },
  },

};


export default webContent;
