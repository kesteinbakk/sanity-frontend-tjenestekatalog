

const whitelistedUsers = {
  name: 'whitelistedUsers',
  title: 'Users allowed to POC',
  type: 'document',
  // In order to limit config to only one document (except when first created)
  // __experimental_actions: [/* 'create' */'update', /* 'delete', */ 'publish'],

  fields: [

    {
      name: 'name',
      title: 'Name:',
      type: 'string',
      validation: Rule => Rule.required(),
    },


    {
      name: 'userMail',
      title: 'User\'s mail:',
      type: 'string',
      validation: Rule => Rule.required(),
    },


  ],



};

export default whitelistedUsers;