

const webPageConfig = {
  name: 'webPageConfig',
  title: 'Web app config',
  type: 'document',
  // In order to limit config to only one document (except when first created)
  __experimental_actions: [/* 'create' */'update', /* 'delete', */ 'publish'],

  fields: [
    {
      name: 'version',
      title: 'Version',
      type: 'string',
      validation: Rule => Rule.required(),
    },
    {
      name: 'contactMail',
      title: 'Contact e-mail',
      type: 'string',
    },

  ],

  preview: {
    prepare: () => ({
      title: 'Config data'
    })
  }

};

export default webPageConfig;