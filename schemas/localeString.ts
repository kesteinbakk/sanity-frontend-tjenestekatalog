//import createSchema from 'part:@sanity/base/schema-creator'
//import schemaTypes from 'all:part:@sanity/base/schema-type'

import { supportedLanguages } from '../settings';


export const localeString = {
  name: 'localeString',
  title: 'Locale string',
  type: 'object',
  // Fieldsets can be used to group object fields.
  // Here we omit a fieldset for the "default language",
  // making it stand out as the main field.
  fieldsets: [
    {
      name: 'translations',
      title: 'Translations',
      options: {
        collapsible: true,
        collapsed: false,
        // columns: 2
      }
    }
  ],
  // Dynamically define one field per language
  fields: supportedLanguages.map((lang) => ({
    name: lang.id,
    title: lang.name,
    type: 'string',
    fieldset: lang.default ? null : 'translations'
  }))
};