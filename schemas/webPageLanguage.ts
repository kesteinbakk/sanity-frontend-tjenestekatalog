import { supportedLanguages } from "../settings";
import { firstUpper } from "../utils";

const webPageLanguage = {

  name: 'webPageLanguage',
  title: 'Web app language',
  type: 'document',
  fields: [
    {
      name: 'name',
      title: 'Name',
      type: 'slug',
      validation: Rule => Rule.required(),
    },
    {
      name: 'group',
      title: 'Group',
      type: 'reference',
      to: [{ type: 'languageGroups' }],
      validation: Rule => Rule.required(),
    },

    {
      name: 'where',
      title: 'Used where',
      type: 'array',
      of: [{ type: 'string' },] //{ type: 'reference', to: [{ type: 'usedWhere' }] }
    },

    {
      name: 'localeValue',
      title: 'Locale content',
      type: 'localeString'
    },

  ],
  orderings: [
    {
      name: "sortAlpha",
      title: 'Alphabetical order',
      by: [{ field: "name.current", direction: "asc" }],
    },
  ],
  preview: {
    select: {
      title: 'name.current',
      value: 'localeValue',
      group: 'group.name',
      where: 'where'
    },
    prepare: ({ title, value, group, where }) => {
      const usedLen = where?.length || 0;
      const groupStr = group ? ` (${firstUpper(group)})` : '';

      return {
        title: `${title}${groupStr}: ${usedLen}`,
        subtitle: supportedLanguages.map(lang => value?.[lang.id] || '').join('/')
      };

    }
  }
};

export default webPageLanguage;