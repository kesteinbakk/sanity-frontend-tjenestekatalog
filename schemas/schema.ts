
import createSchema from 'part:@sanity/base/schema-creator'

import schemaTypes from 'all:part:@sanity/base/schema-type'
import { localeString } from './localeString'
import webPageConfig from './webPageConfig'
import webPageLanguage from './webPageLanguage'
import languageGroups from './languageGroups'
import webContent from './webContent'
import blockContent from './blockContent'
import whitelistedUsers from './whitelistedUsers'
import usedWhere from './usedWhere'


export default createSchema({
  // We name our schema
  name: 'default',
  // Then proceed to concatenate our document type
  // to the ones provided by any plugins that are installed
  types: schemaTypes.concat([
    // Document schemas:
    webPageLanguage,
    webPageConfig,
    languageGroups,
    webContent,
    whitelistedUsers,
    usedWhere,

    // Support schemas:
    localeString,
    blockContent,
  ]),
})
