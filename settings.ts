

type LangId = 'no' | 'en'

type SupportedLanguages = {
  id: LangId,
  name: string,
  default?: boolean
}

// export const defaultLang: LangId = 'no';

export const supportedLanguages: SupportedLanguages[] = [
  {
    id: 'no',
    name: 'Norwegian',
    default: true,
  },
  {
    id: 'en',
    name: 'English',
  }
];

