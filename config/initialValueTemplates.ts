import T from '@sanity/base/initial-value-template-builder';


const initialValueTemplates = [
  ...T.defaults(),
  // Lang by group
  T.template({
    id: 'webPageLanguage-by-group',
    title: "Web page by group",
    schemaType: 'webPageLanguage',
    parameters: [{ name: 'groupId', type: 'string' }],
    //   icon: FcServices,
    value: params => ({
      group: { _ref: params.groupId },
    }),
  }),

  // Lang by usage
  T.template({
    id: 'webPageLanguage-by-usage',
    title: "Web page by Usage",
    schemaType: 'webPageLanguage',
    parameters: [{ name: 'whereId', type: 'string' }],
    //   icon: FcServices,
    value: ({ whereId }) => ({
      where: [whereId],
    }),
  }),
]

export default initialValueTemplates;