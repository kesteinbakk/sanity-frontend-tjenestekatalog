import S from '@sanity/desk-tool/structure-builder';

const webPageLanguageByGroup = S.listItem()
  .title('Web app language by group')
  .id('webPageLanguage-by-group')
  .child(
    S.documentTypeList('languageGroups')
      .title('Language by group')
      .child(groupId =>
        S.documentList()
          .title('Language docs in selected group')
          .filter('_type == "webPageLanguage" && $groupId == group._ref')
          .params({ groupId })
          .initialValueTemplates([
            // @ts-ignore
            S.initialValueTemplateItem('webPageLanguage-by-group', { groupId })
          ])
      )
  );
/* 
const webPageLanguageByUsage = S.listItem()
  .title('Web app language by usage')
  .id('webPageLanguage-by-usage')
  .child(
    S.documentTypeList('usedWhere')
      .title('Language by usage')
      .child(whereId =>
        S.documentList()
          .title('Language docs by usage')
          .filter('_type == "webPageLanguage" && $whereId in where')
          .params({ whereId })
          .initialValueTemplates([
            // @ts-ignore
            S.initialValueTemplateItem('webPageLanguage-by-usage', { whereId })
          ])
      )
  ); */

const deskStructure = () =>
  S.list()
    .title('Frontend data')
    .items([
      S.documentTypeListItem('webPageConfig'),
      S.documentTypeListItem('whitelistedUsers'),

      S.divider(),

      webPageLanguageByGroup,
      //  webPageLanguageByUsage,
      S.documentTypeListItem('webPageLanguage'),

      S.divider(),

      S.documentTypeListItem('webContent'),

      S.divider(),

      S.documentTypeListItem('languageGroups'),
      S.documentTypeListItem('usedWhere'),
    ]);

export default deskStructure;